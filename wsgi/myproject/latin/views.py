# coding=utf-8

import os
import re
from urllib.request import urlopen

from django.shortcuts import render, HttpResponse

from latin.stardict import Dictionary
from latin.vocabs import la2zh

# Create your views here.

base_dir = os.path.dirname(os.path.abspath(__file__))
# print(base_dir)
lewis = Dictionary(base_dir + '/lewis/Lewis Elementary.ifo')


def filter_proc_whitaker(m):
    if m.group(0).startswith('<'):
        return m.group(0)
    else:
        return '<span class="w">' + m.group(0) + '</span>'


def filter_proc_lewis(m):
    p = re.sub('\w+',
               lambda n: '<i><span class="w">' + n.group(0) + '</span></i>',
               m.group(1))
    return p


def get_whitaker(request, query):
    try:
        url = 'http://www.archives.nd.edu/cgi-bin/wordz.pl?keyword={}'.format(query)
        result = urlopen(url).read().decode().split('</pre>')[0].split(
            '<pre>')[1].strip()
        # .replace('\n', '\n<br>').replace(' ', '&nbsp;')
        result = re.sub('[<\w|.>]+', filter_proc_whitaker, result)
        span_result = result.replace('\n', '\n<br>').replace(
            ' ', '&nbsp;').replace('<span&nbsp;', '<span ')
        return HttpResponse(span_result)
    except Exception:
        return HttpResponse('')
        #   bottle  django  return


def get_lewis(request, query):
    try:
        result_gen = lewis[query]
        results = [r.data for r in result_gen]
        results = '<br><br>'.join(results)
        span_results = re.sub('<i>(.*?)</i>', filter_proc_lewis, results)

        try:
            result_zh = la2zh[query]
        except KeyError:
            result_zh = ''

        return HttpResponse(result_zh + '<br>' + span_results)

    except KeyError:
        return HttpResponse('Not Found!')

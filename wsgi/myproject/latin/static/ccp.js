/* eslint-env browser, jquery */

$(document).ready(function () {
  // define jquery elements
  var $button = $('#button')
  var $button2 = $('#button2')
  var $keyword = $('#keyword')
  var $output = $('#output')
  var $lewis = $('#lewis')
  // pagination not used
  // var $pagination = $('#pagination')
  var $clickables

  // define spinner object
  var opts = {
    lines: 11, // The number of lines to draw
    length: 0, // The length of each line
    width: 7, // The line thickness
    radius: 11, // The radius of the inner circle
    scale: 0.75, // Scales overall size of the spinner
    corners: 0, // Corner roundness (0..1)
    top: '36px'
  }
  var spinner = new Spinner(opts)

  // a quick hack to format string prototype
  String.prototype.format = function (args) {
    var result = this
    if (arguments.length < 1) {
      return result
    }

    var data = arguments // if template args are Array
    if (arguments.length === 1 && typeof (args) === 'object') {
      // if template args ar objects
      data = args
    }
    for (var key in data) {
      var value = data[key]
      if (undefined !== value) {
        result = result.replace('{' + key + '}', value)
      }
    }
    return result
  }

  function search (pageParam) {
    // default_page page=1
    if (typeof (pageParam) === 'undefined') pageParam = 1
    if ($keyword[0].value !== '') { // avoid calling self recursively
      $.ajax({
        type: 'GET',
        url: '/latin/whitaker/' + $keyword[0].value,
        beforeSend: function () {
          // show spinner
          spinner.spin($output[0])
        },
        complete: function () {
          // stop spinner
          spinner.stop()
        },
        success: function (data) {
          $output.html(data)
            // $lewis.html('');
          $clickables = $('.w')
          $clickables.click(function () {
            // console.log(this);
            $keyword[0].value = this.innerHTML
          })
        }
      })
    }
  }

  function getLewisWord (query) {
    $.ajax({
      type: 'GET',
      url: '/latin/lewis/' + $keyword[0].value,
      beforeSend: function () {
        // show spinner
        spinner.spin($output[0])
      },
      complete: function () {
        // stop spinner
        spinner.stop()
      },
      success: function (data) {
        $lewis.html(data)
        $clickables = $('.w')
        $clickables.click(function () {
          // console.log(this);
          $keyword[0].value = this.innerHTML
        })
      }
    })
  }

  $button.click(function () {
    search()
  })

  $button2.click(function () {
    getLewisWord()
  })

  // $clickables.click(function () {
  //   // console.log(this);
  //   $keyword[0].value = this.innerHTML;
  // });

  document.onkeydown = function (e) {
    if (!e) e = window.event
    if ((e.keyCode || e.which) === 13) {
      search()
    }
  }
}) // document.ready

"""myproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    # url(r'^$', ListNotes.as_view(), name='listnotes'),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # url(r'^note/$', NoteList.as_view()),
    # url(r'^addnote/$', AddNote.as_view(success_url='/listnotes/'), name='addnote'),
    # url(r'^editnote/(?P<pk>[0-9]+)/$', EditNote.as_view(success_url='/listnotes/'), name='editnote'),
    # # url(r'^addnote/$', 'webnote.views.add_note'),
    # url(r'^listnotes/$', ListNotes.as_view(), name='listnotes'),
    # url(r'^latin/', include('latin.urls')),
]

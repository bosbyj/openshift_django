# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webnote', '0004_auto_20150901_0155'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='language',
            field=models.CharField(max_length=2, choices=[('es', 'Spanish'), ('ja', 'Japanese'), ('la', 'Latin')], default='es'),
        ),
    ]

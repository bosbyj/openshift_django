from django.db import models

# Create your models here.


class Note(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    head = models.CharField(unique=True, max_length=100)
    body = models.TextField()

    SPANISH = 'es'
    JAPANESE = 'ja'
    LATIN = 'la'
    LANGUAGE_CHOICES = (
        (SPANISH, 'Spanish'),
        (JAPANESE, 'Japanese'),
        (LATIN, 'Latin'),
    )
    language = models.CharField(max_length=2,
                                choices=LANGUAGE_CHOICES,
                                default=SPANISH)

    class Meta:
        ordering = ['-created']

from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field, Layout, Submit
from crispy_forms.bootstrap import InlineRadios

from webnote.models import Note

class NoteForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.add_input(Submit('submit', 'Submit'))
        self.helper.layout = Layout(
            Field('head'),
            Field('body'),
            InlineRadios('language'),
        )

    class Meta:
        model = Note
        fields = '__all__'
        #### Disable the use of RadioSelect default rendering
        # widgets = {
        #     'language': forms.RadioSelect()
        # }

# coding=utf-8

import re

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView
from rest_framework import generics, viewsets

from webnote.forms import NoteForm
from webnote.models import Note
from webnote.serializers import NoteSerializer


def beautify(text):
    """...............<br>.....\n
    """
    # ################  ..  ################ #
    p = re.sub('<', '{', text)
    p = re.sub('>', '}', p)

    p = re.sub('\n', '<br>', p)
    # p = re.sub('\d+@', '\n', p)
    # p = re.sub('@', '\t', p)
    # p = re.sub('\n', '', p, count=1)
    p = re.sub('<br>(<br>)+', '<br><br>', p)
    p = re.sub('  ', '__', p)
    # ################  ..  ################ #

    # ################  ..  ################ #
    # ............
    p = re.sub('\s*\[\s*', ' [', p)  # rep _[_
    p = re.sub('\s*\]\s*', '] ', p)  # rep _]_
    p = re.sub('\s*«\s*', ' «', p)  # rep .
    p = re.sub('\s*»\s*', '» ', p)  # rep .
    p = re.sub('\s*.\s*', ' «', p)  # rep «_
    p = re.sub('\s*.\s*', '» ', p)  # rep _»
    p = re.sub('\s*{\s*', ' {', p)  # rep _{_
    p = re.sub('\s*}\s*', '} ', p)  # rep _}_
    p = re.sub('\s*\(\s*', '(', p)  # rep (
    p = re.sub('\s*\)\s*', ')', p)  # rep )
    p = re.sub('\s*.\s*', '(', p)  # rep .
    p = re.sub('\s*.\s*', ')', p)  # rep .

    # ....
    p = re.sub(':\s*', ': ', p)  # rep :_
    p = re.sub('.\s*', ': ', p)  # rep ._
    p = re.sub(';\s*', '; ', p)  # rep ;_
    p = re.sub('.\s*', '; ', p)  # rep ._
    p = re.sub(',\s*', ', ', p)  # rep ,_
    p = re.sub('.\s*', ', ', p)  # rep ._
    p = re.sub('\s*.\s*', '?', p)  # rep .
    p = re.sub('\s*.\s*', '!', p)  # rep .
    p = re.sub('\s*\.\s*', '. ', p)  # rep .
    p = re.sub('. ,', '.,', p)  # rep ._,
    p = re.sub('\.\s\.\s\.', '...', p)  # rep ._._.
    p = re.sub('', '...', p)  # rep

    # ........
    p = re.sub('\s*.\s*', '.', p)  # rep _._
    p = re.sub('\s*.\s*', '.', p)  # rep _._

    # ......
    p = re.sub('\s+\s+', ' ', p)  # del __
    p = re.sub('<br> ', '<br>', p)  # del <br>_
    p = re.sub(' <br>', '<br>', p)  # del _<br>
    p = re.sub('(<br>)+(<br>)+(<br>)+', '<br><br>', p)  # del <br><br><br>
    p = re.sub(':(<br>)+(<br>)+', ':<br>', p)  # rep :<br><br>

    # ~......a...............
    p = re.sub('~ s', '~s', p)  # ~s
    p = re.sub('~ es', '~es', p)  # ~es
    p = re.sub('~ as', '~as', p)  # ~as
    # ################  ..  ################ #

    p = p.strip()
    # .. .... <br>
    p = re.sub('$', '<br>', p)  # ... ..
    p = re.sub('(<br>)+(<br>)+$', '<br>', p)  # 2.....1.

    # ############  ..<br>.\n  ############ #
    p = re.sub('<br>', '\n', p)

    beautified = p
    return beautified


class ListNotes(ListView):
    context_object_name = 'note_list'
    template_name = 'listnotes.html'
    paginate_by = 50
    model = Note


class AddNote(CreateView):
    form_class = NoteForm
    model = Note
    #    fields = ['head', 'body']
    template_name = 'addnote.html'
    success_url = '/listnotes/'
#    success_url = '/listnotes/'
#    def form_valid(self, form):
#        form.instance.body = beautify(self.request.body)
#        return super(AddNote, self).form_valid(form)


class EditNote(UpdateView):
    form_class = NoteForm
    model = Note
    template_name = 'editnote.html'
    success_url = '/listnotes/'


class NoteViewSet(viewsets.ModelViewSet):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer


class NoteList(generics.ListCreateAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer


class NoteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer


def add_note(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/listnotes/')
    form = NoteForm()
    return render(request, 'addnote.html', {'form': form})
